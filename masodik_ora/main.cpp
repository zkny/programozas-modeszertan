#include <iostream>

using namespace std;

int main()
{
    setlocale(LC_ALL, ""); // localizalation, but it doesn't work
    int egesz;
    cout << "Variable without initialization" << endl;
    egesz = 0;
    cout << egesz << endl;
    cout << egesz << endl;
    cout << egesz++ << endl; // post fix
    cout << ++egesz << endl; // pre fix

    cout << "Pre fix loop" << endl;
    while(egesz < 10) {
        cout << ++egesz << endl;
    }
    cout << "Post fix loop" << endl;
    while(egesz < 20) {
        cout << ++egesz << endl;
    }
    cout << "Final value: " << egesz << endl;
    cout << "do while" << endl;
    do {
      cout << --egesz << endl;
    } while(egesz); // runs until its not 0
    cout << "For loop" << endl;
    for(int i = 0; i < 10; i++) {
        cout << "Value and index [" << ++egesz << "] [" << i << "]"<< endl;
    }
    int d = 10;
    for(;d;) {
        cout << "d is: " << --d << endl;
    }

    return 0;
}
