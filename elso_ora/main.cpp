#include <iostream>

using namespace std;

int main()
{
    int elso, masodik;
    cout << "Give me a number!" << endl;
    cin >> elso;
    cout << "Give me another one!" << endl;
    cin >> masodik;
    int sum = elso * masodik;
    cout << "Sum is: " << sum << "!" << endl;
    if (masodik  == 0) {
        cout << "Can't divide by 0";
    } else {
        float div = (float)elso / masodik;
        cout << "Divided too: " << div << endl;
    }
    return 0;
}
